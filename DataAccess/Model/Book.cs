﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DataAccess.Model
{
    public class Book
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Nazev knihy je vyzadovan")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Autor knihy je vyzadovan")]
        public string Author { get; set; }

        [Range(1700, 2200, ErrorMessage = "Rok musi byt v intervalu 1700-2200")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Rok vydani knihy je vyzadovan")]
        public int PublishedYear { get; set; }
        [AllowHtml]
        public string Description { get; set; }

        public string ImageName { get; set; }

    }
}
