﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Model
{
    public class Books
    {
        private static List<Book> books = null;
        private static int counter = 4;

        public static int Counter
        {
            get { return ++counter; }
        }
        public static List<Book> GetFakeList
        {
            get
            {
                if (books == null)
                {
                    books = new List<Book>();
                    books.Add(new Book() { Author = "Jan Novak", Name = "Kucharka", Id = 1, PublishedYear = 2014 });
                    books.Add(new Book() { Author = "Jan Hlupak", Name = "Pohadky", Id = 2, PublishedYear = 2000 });
                    books.Add(new Book() { Author = "Jan Chytrak", Name = "Dedek", Id = 3, PublishedYear = 1995 });
                    books.Add(new Book() { Author = "Jan Somrak", Name = "Hmmmmmm", Id = 4, PublishedYear = 2019 });
                }
                return books;
            }
        }
    }
}
