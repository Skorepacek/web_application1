﻿using DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Class;

namespace WebApplication1.Controllers
{
    public class BooksController : Controller
    {
        // GET: Books
        public ActionResult Index()
        {
            ViewBag.Datum = DateTime.Now.ToShortDateString();
            return View(Books.GetFakeList);
        }
        public ActionResult Detail(int id, bool? zobrazPopis)
        {
            ViewBag.ZobrazPopis = zobrazPopis;
            return View(Books.GetFakeList.Where(b => b.Id == id).FirstOrDefault());
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Add(Book book, HttpPostedFileBase picture)
        {
            if (ModelState.IsValid)
            {
                if (picture != null)
                {
                    if (picture.ContentType == "image/png" || picture.ContentType == "image/jpeg")
                    {
                        //picture.SaveAs(Server.MapPath("~/uploads/book") + picture.FileName);
                        Image image = Image.FromStream(picture.InputStream);
                        if (image.Height > 200 || image.Width > 200)
                        {
                            Image smallImage = ImageHelper.ScaleImage(image, 200, 200);
                            Bitmap b = new Bitmap(smallImage);

                            Guid guid = Guid.NewGuid();
                            string imageName = guid.ToString() + ".jpg";

                            b.Save(Server.MapPath("~/uploads/book/" + imageName), ImageFormat.Jpeg);

                            smallImage.Dispose();
                            b.Dispose();

                            book.ImageName = imageName;
                        }
                        else
                        {
                            picture.SaveAs(Server.MapPath("~/uploads/book/" + picture.FileName));
                        }
                    }
                }
                book.Id = Books.Counter;
                Books.GetFakeList.Add(book);

                TempData["message-success"] = "Kniha byla uspesne pridana";
            }
            else
            {
                return View("Create", book);
            }

            return RedirectToAction("Index");
        }
    }
}